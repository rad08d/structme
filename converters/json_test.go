package converters

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

const JSON = `{
	"quiz": {
			"sport": {
					"q1": {
							"question": "Which one is correct team name in NBA?",
							"options": [
									"New York Bulls",
									"Los Angeles Kings",
									"Golden State Warriros",
									"Huston Rocket"
							],
							"answer": "Huston Rocket"
					}
			},
			"maths": {
					"q1": {
							"question": "5 + 7 = ?",
							"options": [
									"10",
									"11",
									"12",
									"13"
							],
							"answer": "12"
					},
					"q2": {
							"question": "12 - 8 = ?",
							"options": [
									"1",
									"2",
									"3",
									"4"
							],
							"answer": "4"
					}
			}
	}
}
  `

func TestParseJsonElement(t *testing.T) {
	jsonString := jsonToMap(JSON)
	elements := parseJSONMap("fake", jsonString, make(map[string]Element))
	// Check that all elements exist in map
	_, ok := elements["quiz"]
	assert.True(t, ok)
	_, ok = elements["sport"]
	assert.True(t, ok)
	_, ok = elements["q1"]
	assert.True(t, ok)
	_, ok = elements["q2"]
	assert.True(t, ok)
	_, ok = elements["maths"]
	assert.True(t, ok)
	_, ok = elements["not_present"]
	assert.False(t, ok)
}

func TestJSONToStruct(t *testing.T) {
	source := JSONToStruct(JSON, "payload")
	// Check that it actually put out some type of string...
	assert.True(t, len(source) > 1)
}