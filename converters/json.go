package converters

import (
	"bytes"
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
	"text/template"
)

const structTemplate = `
type {{.Name}} struct { {{range $name, $type := .Fields }}
	{{$name | Title}} {{$type}} ` + "`json:\"{{$name}}\"`" +
	`{{end}}
}`

// Element is the basic building block of the structs to be created
type Element struct {
	Name   string
	Fields map[string]string
}

// JSONToStruct will convert a JSON string into the relevant structs that make up the JSON
func JSONToStruct(json string, name string) string {
	t, err := prepareTemplate()
	if err != nil {
		panic(err)
	}
	elements := make(map[string]Element)
	parseJSONMap(name, jsonToMap(json), elements)
	var buf bytes.Buffer
	for _, v := range elements {
		buf.WriteString(v.elementToStruct(t))
	}
	return buf.String()
}

func (e *Element) elementToStruct(t *template.Template) string {
	var buf bytes.Buffer
	if err := t.Execute(&buf, e); err != nil {
		panic(err)
	}
	return buf.String()
}

func prepareTemplate() (*template.Template, error) {
	funcMap := template.FuncMap{
		"Title": strings.Title,
	}
	t := template.New("structTemp")
	t.Funcs(funcMap)
	t, err := t.Parse(structTemplate)
	return t, err
}

func parseJSONMap(key string, json map[string]interface{}, elements map[string]Element) map[string]Element {
	element := Element{Name: strings.Title(key), Fields: make(map[string]string)}
	for k, v := range json {
		t := reflect.TypeOf(v).Kind()
		switch t {
		case reflect.Slice:
			// inspect JSON array
			arrayType := inspectJSONArray(v.([]interface{}))
			if arrayType == reflect.Map {
				// this will be a slice of the next struct to be created
				element.Fields[k] = strings.Title(fmt.Sprintf("[]%s", k))
				for _, value := range v.([]interface{}) {
					parseJSONMap(k, value.(map[string]interface{}), elements)
				}
			} else {
				element.Fields[k] = fmt.Sprintf("[]%s", arrayType.String())
			}
		case reflect.Map:
			parseJSONMap(k, v.(map[string]interface{}), elements)
		default:
			// normal key -> value pair
			element.Fields[k] = t.String()
		}
	}
	elements[key] = element
	return elements
}

func inspectJSONArray(array []interface{}) reflect.Kind {
	if len(array) > 0 {
		initType := reflect.TypeOf(array[0]).Kind()
		for _, element := range array {
			if reflect.TypeOf(element).Kind() != initType {
				// The types mismatch so just make it the interface type
				return reflect.Interface
			}
		}
		return initType
	}
	// this is an empty array so return a slice type
	return reflect.Slice
}

func jsonToMap(jsonBody string) map[string]interface{} {
	jsonBytes := []byte(jsonBody)
	container := make(map[string]interface{})
	json.Unmarshal(jsonBytes, &container)
	return container
}
