package cmd

import (
	"log"
	"github.com/spf13/cobra"
)

var (
	inFilePath string
	outFilePath = "structme.go"
	endpoint string
)

var (
	rootCmd = &cobra.Command{
		Use:   "structme",
		Short: "Struct Me is a tool to take raw JSON and turn it into Golang structs",
		Long:  "A raw JSON to Golang struct converter. More info here: https://gitlab.com/users/rad08d/structme",
	}

	structMeCmd = &cobra.Command{
		Use:   "parse",
		Short: "Parse will parse a JSON in a local file into the corresponding Golang structs.",
		Long:  "Parse will parse a JSON in a local file into the corresponding Golang structs and save it to a file in the current working directory.",
		Run: func(cmd *cobra.Command, args []string) {
			structMe(inFilePath, outFilePath)
		},
	}

	structMeURLCmd = &cobra.Command{
		Use:   "parseUrl",
		Short: "Parse JSON data found at URL into the corresponding Golang structs.",
		Long:  "Parse JSON data found at URL into the corresponding Golang structs and save it to a file in the current working directory.",
		Run: func(cmd *cobra.Command, args []string) {
			structMeURL(endpoint, outFilePath)
		},
	}
)

func init() {
	rootCmd.AddCommand(structMeCmd)
	structMeCmd.Flags().StringVar(&inFilePath, "file", "", "The file path to the JSON to be parsed")
	structMeCmd.MarkFlagRequired("file")

	rootCmd.AddCommand(structMeURLCmd)
	structMeURLCmd.Flags().StringVar(&endpoint, "url", "", "The endpoint to request data JSON data from to be parsed")
	structMeURLCmd.MarkFlagRequired("url")
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
