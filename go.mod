module gitlab.com/rad08d/jsonconv

require (
	github.com/mitchellh/go-homedir v1.0.0 // indirect
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1
	github.com/stretchr/testify v1.2.2
)
