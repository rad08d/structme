package main

import (
	"gitlab.com/rad08d/structme/cmd"
)

func main() {
	cmd.Execute()
}